import 'dart:math';

import 'package:flutter/material.dart';
import 'package:xml2json/xml2json.dart';
import 'package:localstorage/localstorage.dart';

class ConverterView extends StatefulWidget {
  @override
  _ConverterViewState createState() => _ConverterViewState();
}

class _ConverterViewState extends State<ConverterView> {
  TextEditingController _inputController = TextEditingController();
  TextEditingController _outputController = TextEditingController();

  String converterType = 'Badgerfish';

  final storage = LocalStorage('xml2jsonwebconvertercache');

  _convertAction() {
    final input = _inputController.text;
    storage.setItem('xml2jsonwebconvertercache', input);
    final converter = Xml2Json();
    converter.parse(input);
    String xml;
    try {
      switch (converterType) {
        case 'Badgerfish':
          xml = converter.toBadgerfish();
          break;
        case 'GData':
          xml = converter.toGData();
          break;
        case 'Parker':
          xml = converter.toParker();
          break;
      }
    } catch (e) {
      xml = e.toString();
    }
    setState(() {
      _outputController.text = xml;
    });
  }

  @override
  void initState() {
    storage.ready.then((b) {
      setState(() {
        _inputController.text = storage.getItem('xml2jsonwebconvertercache');
      });
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Json2Xml Web Converter'),
      ),
      body: FutureBuilder<bool>(
          future: storage.ready,
          builder: (context, snapshot) {
            if (!snapshot.hasData) {
              return Center(child: CircularProgressIndicator());
            }
            return ListView(
              padding: EdgeInsets.symmetric(
                horizontal:
                    max(0, (MediaQuery.of(context).size.width - 800) / 2),
                vertical: 16.0,
              ),
              children: <Widget>[
                TextField(
                  controller: _inputController,
                  minLines: 10,
                  maxLines: 100,
                  autofocus: true,
                  autocorrect: false,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'Enter some XML',
                    hintText: '<foo>bar</foo>',
                  ),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    DropdownButton<String>(
                      value: converterType,
                      icon: Icon(Icons.arrow_downward),
                      iconSize: 24,
                      elevation: 16,
                      style: TextStyle(color: Colors.deepPurple),
                      underline: Container(
                        height: 2,
                        color: Colors.deepPurpleAccent,
                      ),
                      onChanged: (String newValue) {
                        setState(() {
                          converterType = newValue;
                        });
                      },
                      items: <String>['Badgerfish', 'GData', 'Parker']
                          .map<DropdownMenuItem<String>>((String value) {
                        return DropdownMenuItem<String>(
                          value: value,
                          child: Text(value),
                        );
                      }).toList(),
                    ),
                    SizedBox(width: 16),
                    RaisedButton(
                      child: Text('CONVERT TO JSON'),
                      onPressed: _convertAction,
                    ),
                  ],
                ),
                TextField(
                  controller: _outputController,
                  minLines: 10,
                  maxLines: 100,
                  readOnly: true,
                  decoration: InputDecoration(
                    border: OutlineInputBorder(),
                    labelText: 'JSON output',
                  ),
                ),
              ],
            );
          }),
    );
  }
}
